from copy import deepcopy
import time
import sys
import matplotlib.pyplot as plt
import numpy as np
import random
import itertools
from IPython import display

class Board(object):
    
    """ A TicTacToe Board. """
    def __init__(self, choose_random = True, win_pos = np.nan):
        self.fields = np.array(9*[0])
        self.num_moves = sum(abs(self.fields))
        if choose_random:
            np.random.seed(1)
            # contains all choose(9,3) == 84 possible combinations of three numbers between 0 and 8
            all_combinations_of_three = list(itertools.combinations(range(9), 3))
            #randomly choose 8 of those
            self.winning_positions = list(all_combinations_of_three[i] for i in np.random.randint(0, 83, 8))
            print self.winning_positions
        else:
            self.winning_positions = win_pos
            
    def reset(self):
        self.fields = np.array(9*[0])
        self.num_moves = 0

class Game(object):
    def __init__(self):
        self.prob = []
        self.game_state = []
        self.action = []
        self.final_game_state = []
        self.result = 0
        
    def reset(self):
        self.prob = []
        self.game_state = []
        self.action = []
        self.final_game_state = []
        self.result = 0
    
    def __str__(self):
        return self.prob
        
class Strategy(object):
    def __init__(self):
        self.table = {}

class Player(object):
    """ A Player. """
    def __init__(self, name):
        self.name = name
    
    def reset(self):
        self.game.reset()
         
    def action(self, state, winningpos):
        pass
        
    def end_game(self, state, result):
        pass
    
    def update_strategy(self, num_games):
        pass

class StrategicPlayer(Player):
    """ A player with different strategies. """
    #has an object strategy, which is a look-up table and defines all actions
    def __init__(self, name, cre=0):
        self.name = name
        self.game = Game()
        self.history = History()
        self.strategy = Strategy()
        self.rangeeight = np.array([0,1,2,3,4,5,6,7,8]) 
        
    def hash_state(self,state):
        return np.array_str(state)
    
    def action(self, state, winningpos):
        #print self.strategy.table
        hstate = self.hash_state(state)
        if hstate in self.strategy.table:
            #print 'I have seen the state before!'
            pr = self.strategy.table[hstate] #vector of probabilities
        else:
            pr = np.zeros(9)
            emptyfields = np.array(np.where(state == 0)).flatten()
            pr[emptyfields] = 1/float(len(emptyfields))
            self.strategy.table[hstate] = pr
            # print pr
            
        act = np.random.choice(self.rangeeight, p = pr)
        
        #print self.strategy.table
        self.game.prob.append(pr[act])
        self.game.game_state.append(state)
        self.game.action.append(act)
        return act
 

    def end_game(self, state, result):
        self.game.final_game_state = state
        self.game.result = result
        self.history.add_game(self.game)
        self.update_strategy()
        self.game = Game()
        # do sth with the history
        
    def get_voisins(self,state):
        voisins = []
        emptyfields = np.array(np.where(state==0)).flatten()
        for e in emptyfields:
            voisins.append(e)
        return voisins

    def update_strategy(self):
        pass

class ExploreExploitPlayer(StrategicPlayer):
    """ A player with different strategies. """
    #has an object strategy, which is a look-up table and defines all actions
    def __init__(self, name, cre=0):
        self.name = name
        self.game = Game()
        self.history = History()
        self.strategy = Strategy()
        self.rangeeight = np.array([0,1,2,3,4,5,6,7,8]) 
    
    def update_strategy(self, num_games):
        if ((num_games % 500 == 0) and ((game_id < 502) and (game_id > 0))):
            print '...updating strategy...'
    
            cum_gain_action_taken = dict(zip(self.strategy.table.keys(),
                                             [np.zeros(9) for k in range(len(self.strategy.table.keys()))]))
            
            # go through all games in history
            for g in self.history.game:
                # go through all game states in game g
                for decisions_game_i in range(len(g.game_state)):
                    #print g.game_state[decisions_game_i] 
                    # get hashed game state
                    a = self.hash_state(g.game_state[decisions_game_i])              
                    ac = g.action[decisions_game_i]
                    cum_gain_action_taken[a][ac] = cum_gain_action_taken[a][ac] + g.result #remove for assignment

            #print cum_gain_action_taken        
            for game_state_hashed in self.strategy.table.keys():
                self.strategy.table[game_state_hashed] = np.zeros(9) #remove for assignment
                gs = game_state_hashed
                c = gs.replace('[', '').replace(']','')
                d = np.fromstring(c, sep = " ")
                #print '----'
                #print game_state_hashed
                nonzerooos = np.array(np.where(d != 0)).flatten()
                #print nonzerooos
                #print cum_gain_action_taken[game_state_hashed]
                cum_gain_action_taken[game_state_hashed][nonzerooos] = -np.inf
                #print cum_gain_action_taken[game_state_hashed]
                bestact = np.argmax(cum_gain_action_taken[game_state_hashed])
                #print bestact
                self.strategy.table[game_state_hashed][bestact] = 1
                #print self.strategy.table[game_state_hashed]
                
class ColombianPlayer(StrategicPlayer):
    
    def __init__(self, lr=0.3, temp=0.01):
        self.name = "Colombian"
        self.game = Game()
        self.history = History()
        self.strategy = Strategy()
        self.value = Strategy()
        self.lr = lr
        self.temp = temp
        self.rangeeight = np.array([0,1,2,3,4,5,6,7,8])
        
    def get_voisins(self,state):
        voisins = []
        emptyfields = np.array(np.where(state==0)).flatten()
        for e in emptyfields:
            voisins.append(e)
        return voisins
        
    def update_strategy(self, n_games):
        game = self.history.game[-1]
        
        visited_states = game.game_state

        last_state = visited_states[-1]
        outcome = game.result
        hstate = self.hash_state(last_state)
        
        if outcome ==1: self.value.table[hstate] = 1
        if outcome == -1: self.value.table[hstate] = 0
        if outcome == 0:
            if hstate not in self.value.table: self.value.table[hstate] = 0.5

        last_value = self.value.table[hstate]

        for past_s in range(len(visited_states)-2,-1,-1):
            
            st = np.copy(visited_states[past_s])
            hstate = self.hash_state(st)

            if hstate not in self.value.table:
                self.value.table[hstate] = 0.5

            self.value.table[hstate] += self.lr*(last_value-self.value.table[hstate])
            last_value = self.value.table[hstate]

            voisins = self.get_voisins(st)

            vs = []
            for v in voisins:
                n_state = np.copy(st)
                n_state[v] = 1
                max_val = 0
                for u in voisins:
                    if u!=v:
                        n_state[u] = -1

                        hstate_temp = self.hash_state(n_state)
                
                        if hstate_temp not in self.value.table:
                            self.value.table[hstate_temp] = 0.5
                        max_val = max(max_val, self.value.table[hstate_temp])
                        n_state[u] = 0

                vs.append(max_val)
                n_state[v] = 0
                
            pr = np.zeros(9)
            vs = np.array(vs)
            temp = self.temp
            norm = np.sum(np.exp(vs/temp))
            ind = 0
            for v in voisins:
                pr[v] = np.exp(vs[ind]/temp)/norm
                ind += 1

            self.strategy.table[hstate] = pr
                
    
class ExploreExploitPlayer(StrategicPlayer):
    """ A player with different strategies. """
    #has an object strategy, which is a look-up table and defines all actions
    def __init__(self, name, cre=0):
        self.name = name
        self.game = Game()
        self.history = History()
        self.strategy = Strategy()

        
    def update_strategy(self):
        #update
        pass

class RandomPlayer(Player):
    """ Random, a player with a predefined (completely random) strategy. """
    def __init__(self):
        self.name = "Rando M."
        
    def action(self, state, winningpos):
        emptyfields = np.array(np.where(state == 0)).flatten()
        if len(emptyfields) == 1:
            ind = 0
        else:
            ind = np.random.randint(0, sum(state == 0)-1)    
        ret = emptyfields[ind]
        return ret

class AlwaysLeftPlayer(Player):
    """ A player who always puts a mark on the first free field. """
    def __init__(self):
        self.name = "Lefto"
        
    def action(self, state, winningpos):
        emptyfields = np.array(np.where(state == 0)).flatten()
        ret = emptyfields[0]
        return ret 

class HumanPlayer(Player):
    def __init__(self, name="Human"):
        self.name = name
        
    
    def action(self, state, winningpos):
        response = "a"
        print("The current position is: " + str(state) + ".")
        s = "where do you want to make the next cross?"
        while type(response) != int:
            response = input(self.name + ", " + s + " (number between 0 and 8): ")
        return response

class History(object):
    def __init__(self):
        self.game = []
        
    def add_game(self, g):
        self.game.append(deepcopy(g)) 
        
    def remove_almost_all_games(self, nn):
        self.game = self.game[(-nn):(-1)]


class Table(object):
    def __init__(self, player1, player2):
        self.player1 = player1
        self.player2 = player2
        self.player1_won = 0
        self.player2_won = 0
        self.credits = []
        self.output = True
        self.board = Board()
        self.game_is_on = True
        self.num_games = 0
    
    def reset(self):
        self.board.reset()
        self.game_is_on = True

    def play_a_game(self, game_id):
        self.reset()
        
        while self.game_is_on:
            if (self.board.num_moves + game_id)%2 == 0: #player1's turn
                
                a = self.player1.action(1 * self.board.fields, self.board.winning_positions)
                #if self.output:
                    #print(self.player1.name + " decided to play " + str(a))
                if (a<9 and 0<=a ):
                    if (self.board.fields[a] == 0):
                        self.board.fields[a] = 1
                    else:
                        raise NameError('This field is already occupied!!')
                else:
                    raise NameError('The fields are numbered from 0 to 8!')
                    
            else: #player 2's turn
                a = self.player2.action(-1 * self.board.fields, self.board.winning_positions)
                #if self.output:
                    #print(self.player2.name + " decided to play " + str(a))
                if (a<9 and 0<=a):
                    if (self.board.fields[a] == 0):
                        self.board.fields[a] = -1 
                    else:    
                        raise NameError('This field is already occupied!!')
                else:
                    raise NameError('The fields are numbered from 0 to 8!')
            self.board.num_moves += 1
            self.evaluate_board()
            
    
    def end_game(self, result):
        if self.num_games == 0:
            self.credits = np.array([self.player1_won,self.player2_won])
        else:
            self.credits = np.vstack((self.credits, [self.player1_won,self.player2_won]))
        #self.game = Game()
        self.num_games += 1
        self.game_is_on = False
        self.player1.end_game(self.board.fields, result)
        self.player2.end_game(-self.board.fields, -result) 
        
        
    def evaluate_board(self):
        # if less than 5 fields are occupied, keep playing.
        if self.board.num_moves < 5:
            return 0
        else:
            #print self.board.num_moves
            for i in range(7):
                cur_win_pos = self.board.winning_positions[i]
                field_evaluated = sum( [self.board.fields[j] for j in cur_win_pos ])
                if (self.game_is_on and field_evaluated == 3):
                    self.player1_won += 1
                    #if self.output:
                        #print self.player1.name + " won! Winning position: " + str(cur_win_pos)
                    self.end_game(1) #player1 won!
                                        
                elif (self.game_is_on and field_evaluated == -3):
                    self.player2_won += 1
                    #if self.output:
                        #print self.player2.name + " won! Winning position: " + str(cur_win_pos)
                    self.end_game(-1) #player2 won!
                    
            if (self.game_is_on and (self.board.num_moves == 9)):
                #if self.output: 
                    #print "draw!"
                self.end_game(0) #draw!

                


random.seed(2)

do_plot = False

n_games = 1000
# create and add players
p1 = ColombianPlayer()
#p2 = RandomPlayer()
p2 =RandomPlayer()
p1 = ExploreExploitPlayer
#p1.output = True

#p1 = RandomPlayer()
#p2 = AlwaysLeftPlayer()

Credits = np.zeros((2,n_games))

tableETH = Table(p1, p2)
tableETH.output = True

do_plot = False
if(do_plot):
    #plt.close("all")
    plt.axis([0, n_games, -10, 1000])
    lines = [plt.plot([], [])[0] for _ in range(2)]

won_1, won_2 = [], []

for game_id in range(n_games):
    #if tableETH.output:
        #print("\n\n ---- Game number " + str(game_id) + " is being played! ----")
    #else: 
        #print "\rYou have finished %d games" % game_id,
        #sys.stdout.flush()
    
    #play game
    tableETH.play_a_game(game_id)
    #print "Board: " + str(tableETH.board.fields)
    won_1.append(tableETH.player1_won)
    won_2.append(tableETH.player2_won)    

    if(do_plot):
        # Update plot every 20 games
        if ((game_id > 0) and (game_id % 10-1 == 0)):
            Credits = np.transpose(tableETH.credits)
            if (np.min(Credits) < plt.gca().get_ylim()[0]):
                plt.gca().set_ylim([np.min(Credits)-10,100])
            for i in range(2):
                lines[i].set_xdata(range(game_id+1))
                lines[i].set_ydata(Credits[i,0:(game_id+1)])
            plt.draw()
            time.sleep(0.1)
        plt.show()

plt.plot(won_1, 'blue')
plt.plot(won_2, 'red')
plt.show()
print "\n"
print "Player", p1.name, " has won ", tableETH.player1_won, " games."
print "Player", p2.name, " has won ", tableETH.player2_won, " games." 
